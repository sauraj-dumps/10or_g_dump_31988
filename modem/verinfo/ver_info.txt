{
    "Image_Build_IDs": {
        "adsp": "ADSP.8953.2.8.2-00056-00000-1", 
        "apps": "LA.UM.5.6.r1-04600-89xx.0-1.108752.1.109133.1", 
        "boot": "BOOT.BF.3.3-00214-M8917LAAAANAZB-1", 
        "common": "MSM8953.LA.2.0-00540-STD.PROD-1.109682.1.111554.1", 
        "cpe": "CPE.TSF.1.0-00035-W9335AAAAAAAZQ-1", 
        "glue": "GLUE.MSM8953.2.0-00006-NOOP_TEST-1", 
        "modem": "MPSS.TA.2.3.c1-00361-8953_GEN_PACK-1.109133.1.109682.1", 
        "rpm": "RPM.BF.2.4-00054-M8953AAAAANAZR-1", 
        "tz": "TZ.BF.4.0.5-00036-M8937AAAAANAZT-1", 
        "video": "VIDEO.VE.4.2-00049-PROD-1", 
        "wcnss": "CNSS.PR.4.0-00374-M8953BAAAANAZW-1.109682.1.111554.1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "MSM8953.LA.2.0-00540-STD.PROD-1.109682.1.111554.1", 
        "Product_Flavor": "asic", 
        "Time_Stamp": "2017-09-13 21:12:55"
    }, 
    "Version": "1.0"
}